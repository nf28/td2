package Controller;

import Model.Contact;
import Model.Country;
import javafx.collections.MapChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.converter.NumberStringConverter;

import java.util.HashMap;

public class Controller {

    private Contact contact;

    @FXML
    private TextField firstName = new TextField();
    @FXML
    private TextField lastName = new TextField();
    @FXML
    private TextField addressName = new TextField();
    @FXML
    private TextField postcode = new TextField();
    @FXML
    private TextField city = new TextField();
    @FXML
    private ComboBox<String> countryComboBox;
    @FXML
    private DatePicker birthDate = new DatePicker();
    @FXML
    private ToggleGroup sex;
    @FXML
    private RadioButton male = new RadioButton();
    @FXML
    private RadioButton female = new RadioButton();
    @FXML
    private Button validate;

    private MapChangeListener<String,String> listener;

    private HashMap<String, Control> controlsMap;

    public Controller () {
        this.contact = new Contact();
        this.controlsMap = new HashMap<String, Control>();
    }

    @FXML
    public void initialize(){
        countryComboBox.setItems(Country.getCountries());

        // Binding view -> model
        this.firstName.textProperty().bindBidirectional(this.contact.firstNameProperty());
        this.lastName.textProperty().bindBidirectional(this.contact.lastNameProperty());
        this.addressName.textProperty().bindBidirectional(this.contact.getAddress().addressNameProperty());
        this.postcode.textProperty().bindBidirectional(this.contact.getAddress().postcodeProperty(), new NumberStringConverter());
        this.city.textProperty().bindBidirectional(this.contact.getAddress().cityProperty());

        this.countryComboBox.getSelectionModel().selectedItemProperty().addListener((obs, oldV, newV) -> {
            this.contact.getAddress().setCountry(newV);
            System.out.println("Changed");
        });

        this.sex.selectedToggleProperty().addListener((obs, oldV, newV) -> {
            RadioButton radio = (RadioButton) sex.getSelectedToggle();
            this.contact.setSex(radio.getText());
        });

        this.birthDate.valueProperty().addListener((obs, oldV, newV) -> {
            this.contact.setBirthDate(newV);
        });

        // validation
        this.controlsMap.put(this.contact.firstNameProperty().getName(), this.firstName);
        this.controlsMap.put(this.contact.lastNameProperty().getName(), this.lastName);
        this.controlsMap.put(this.contact.getAddress().addressNameProperty().getName(), this.addressName);
        this.controlsMap.put(this.contact.getAddress().postcodeProperty().getName(), this.postcode);
        this.controlsMap.put(this.contact.getAddress().cityProperty().getName(), this.city);
        this.controlsMap.put(this.contact.getAddress().countryProperty().getName(), this.countryComboBox);
        this.controlsMap.put(this.contact.birthDateProperty().getName(), this.birthDate);

        listener = changed -> {
            if(changed.wasAdded()) {
                if(changed.getKey() == this.contact.sexProperty().getName()) {
                    this.male.setStyle("-fx-border-color: red ;");
                    this.female.setStyle("-fx-border-color: red ;");
                    this.male.setTooltip(new Tooltip(changed.getValueAdded()));
                    this.female.setTooltip(new Tooltip(changed.getValueAdded()));
                } else {
                    this.controlsMap.get(changed.getKey()).setStyle("-fx-border-color: red ;");
                    this.controlsMap.get(changed.getKey()).setTooltip(new Tooltip(changed.getValueAdded()));
                }
            } else {
                if(changed.getKey() == this.contact.sexProperty().getName()) {
                    this.male.setStyle("-fx-border-color: null ;");
                    this.female.setStyle("-fx-border-color: null ;");
                    this.male.setTooltip(null);
                    this.female.setTooltip(null);
                } else{
                this.controlsMap.get(changed.getKey()).setStyle("-fx-border-color: null ;");
                this.controlsMap.get(changed.getKey()).setTooltip(null);
                }
            }
        };

        this.contact.getErrors().addListener(listener);

        this.validate.setOnAction(evt -> {
            this.contact.validate();
        });

    }
}