package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Locale;

public class Country {

    static public ObservableList<String> getCountries() {

        String[] isoCountries = Locale.getISOCountries();

        ObservableList<String> countries = FXCollections.observableArrayList();

        for (String isoCountry : isoCountries) {
            Locale locale = new Locale("FRENCH", isoCountry);
            countries.add(locale.getDisplayCountry());
        }

        return countries;
    }
}
