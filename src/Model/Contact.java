package Model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.time.LocalDate;

public class Contact {

    private StringProperty lastName;
    private StringProperty firstName;
    private Address address;
    private ObjectProperty<LocalDate> birthDate;
    private StringProperty sex;

    private ObservableMap<String,String> errors;

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public LocalDate getBirthDate() {
        return birthDate.get();
    }

    public ObjectProperty<LocalDate> birthDateProperty() {
        return birthDate;
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public Address getAddress() {
        return address;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate.set(birthDate);
    }

    public String getSex() {
        return sex.get();
    }

    public StringProperty sexProperty() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public Contact() {
        this.lastName = new SimpleStringProperty(this, "lastName", null);
        this.firstName = new SimpleStringProperty(this, "firstName", null);
        this.address = new Address();
        this.birthDate = new SimpleObjectProperty<LocalDate>(this, "birthDate", null);
        this.sex = new SimpleStringProperty(this, "sex", null);

        this.errors = FXCollections.observableHashMap();
    }

    public ObservableMap<String, String> getErrors() {
        return errors;
    }

    public void validate() {
        if (this.lastName.getValue() == null) {
            errors.put(lastName.getName(), "Lastname is mandatory");
        } else if (errors.get(lastName.getName()) != null) {
            errors.remove(lastName.getName());
        }

        if (this.firstName.getValue() == null) {
            errors.put(firstName.getName(), "Firstname is mandatory");
        } else if (errors.get(firstName.getName()) != null) {
            errors.remove(firstName.getName());
        }

        if (this.getAddress().addressNameProperty().getValue() == null) {
            errors.put(this.getAddress().addressNameProperty().getName(), "Address name is mandatory");
        } else if (errors.get(this.getAddress().addressNameProperty().getName()) != null) {
            errors.remove(this.getAddress().addressNameProperty().getName());
        }

        if (this.getAddress().postcodeProperty().getValue() == null) {
            errors.put(this.getAddress().postcodeProperty().getName(), "Postcode is mandatory");
        } else if (errors.get(this.getAddress().postcodeProperty().getName()) != null) {
            errors.remove(this.getAddress().postcodeProperty().getName());
        }

        if (this.getAddress().cityProperty().getValue() == null) {
            errors.put(this.getAddress().cityProperty().getName(), "City is mandatory");
        } else if (errors.get(this.getAddress().cityProperty().getName()) != null) {
            errors.remove(this.getAddress().cityProperty().getName());
        }

        if (this.getAddress().countryProperty().getValue() == null) {
            errors.put(this.getAddress().countryProperty().getName(), "Country is mandatory");
        } else if (errors.get(this.getAddress().countryProperty().getName()) != null) {
            errors.remove(this.getAddress().countryProperty().getName());
        }

        if (this.birthDate.getValue() == null) {
            errors.put(birthDate.getName(), "Birthdate is mandatory");
        } else if (errors.get(birthDate.getName()) != null) {
            errors.remove(birthDate.getName());
        }

        if (this.sex.getValue() == null) {
            errors.put(sex.getName(), "Sex is mandatory");
        } else if (errors.get(sex.getName()) != null) {
            errors.remove(sex.getName());
        }

    }

}
